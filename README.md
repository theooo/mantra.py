![mantra.py](.assets/mantra.webp)

# mantra.py

<img src=".assets/demo.gif">

View online manual pages (from https://man.archlinux.org) from the terminal.  
Rewrite of [mantra](https://gitlab.com/chanceboudreaux/mantra) in Python.

## Note

`mantra` is never meant to be a `man` alternative. `mantra` is a tool to view manpages of a command without downloading and installing the whole package.

## Dependencies

- Python
- [BeautifulSoup4](https://pypi.org/project/beautifulsoup4/)
- [fzf](https://github.com/junegunn/fzf)

## Installation

- Manual way:

```
git clone https://codeberg.org/theooo/mantra.py.git
cd mantra.py/
sudo make install
```

or

```
sudo curl -sL "https://codeberg.org/theooo/mantra.py/raw/branch/main/mantra" -o /usr/local/bin/mantra 
sudo chmod +x /usr/local/bin/mantra
```

- AUR:

```
yay -S mantra
```

## Usage

```
usage: mantra.py [-h] [-k] [-d]

View online manual pages (from https://man.archlinux.org) from the terminal.

options:
  -h, --help       show this help message and exit
  -k , --keyword   keyword
  -d, --download
```
